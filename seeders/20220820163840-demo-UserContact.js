'use strict';

module.exports = {
  up:(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
     return queryInterface.bulkInsert('UserContacts', [{
      UserId: '1',
      contactName: 'sirisha Father',
      contactNumber:"9676474931",
      contactEmail:"sirishafather@gmail.com",
      createdAt:new Date(),
      updatedAt:new Date()
    },{
      UserId: '1',
      contactName: 'sirisha Mother',
      contactNumber:"95505629610 ",
      contactEmail:"sirishamother@gmail.com",
      createdAt:new Date(),
      updatedAt:new Date()
    }], {});
  },

  down:(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('UserContacts', null, {});
  }
};
