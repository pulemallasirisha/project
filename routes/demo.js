var express = require('express');
var router = express.Router();
var userController=require('../controller/usercontroller');
//    GET home page.*/
router.get('/',userController.getAll);
router.get('/withcontact',userController.getAllWithContacts);
router.get('/:id',userController.findOne);
 
module.exports = router; 