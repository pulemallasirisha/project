var Model = require('../models');

module.exports.getAll = function (req, res) {
    Model.User.findAll()
    .then(function (users) {
        res.send(users);
    });
}

//retrive by one
module.exports.findOne = (req, res) => {

    const id = req.params.id;
  
   
  
      Model.User.findByPk(id)
  
   
  
        .then(data => {
  
   
  
          if (data) {
  
   
  
            res.send(data);
  
   
  
          } else {
  
   
  
            res.status(404).send({
  
   
  
              message: `Cannot find users with id=${id}.`
  
   
  
            });
  
   
  
          }
  
   
  
        })
  
   
  
        .catch(err => {
  
   
  
          res.status(500).send({
  
   
  
            message: "Error retrieving users with id=" + id
  
   
  
          });
  
   
  
        });
  
   
  
    };

module.exports.getAllWithContacts= function (req, res) {
    Model.User.findAll({ include: Model.UserContact})
    .then(function (users) {
        res.send(users);
    });
}
  

// module.exports = router ;