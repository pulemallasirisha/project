'use strict';

module.exports = {
  up:(queryInterface, Sequelize) =>  {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    return queryInterface.addColumn('Users', 'isActive', 
      { type: Sequelize.DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true 
      });
  },

  down:(queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.removeColumn('users', 'isActive'); 
  }
};
